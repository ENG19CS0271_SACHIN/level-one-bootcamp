//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
int main()
{
   int x1, x2 , x;
   printf("Enter the x coordinates = ");
   scanf("%d %d",&x1, &x2);
    x = (x2-x1);
   int y1, y2 , y;
   printf("Enter the y coordinates = ");
   scanf("%d %d",&y1, &y2);
   y = (y2-y1);
   int distance = sqrt(x*x + y*y);
   printf("Distance = %d", distance);
   return 0;
}
