#include<stdio.h>
struct fract
{
int nume;
int deno;
};
typedef struct fract fraction;
fraction input()
{
fraction p;
printf("Enter the numerator value\n");
scanf("%d",&p.nume);
printf("Enter the denominator value\n");
scanf("%d",&p.deno);
return p;
}
int gcd(int p,int q)
{
int r=1;
for(int i=2;i<=p && i<=q;i++)
{
if(p%i==0 && q%i==0)
{
r=i;
}
}
return r;
}
fraction sum(fraction p, fraction q)
{
fraction res;
res.deno=p.deno*q.deno;
res.nume=(p.nume*q.deno)+(q.nume*p.deno);
int r=gcd(res.nume,res.deno);
res.nume=res.nume/r;
res.deno=res.deno/r;
return res;
}
void output(fraction p,fraction q,fraction s)
{
printf("the sum of %d / %d and %d / %d is %d / %d",p.nume,p.deno,q.nume,q.deno,s.nume,s.deno);
}
int main()
{
fraction p,q,s;
p=input();
q=input();
s=sum(p,q);
output(p,q,s);
return 0;
}